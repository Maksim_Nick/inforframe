$(function() {
function popupInit(){
        
        var tm_popup=null,
            tm_popupDuration=30,
            tm_popupFadeDuration=300;
            
        $('[data-popup-open]').click(function(e){
        
            e.preventDefault();
            
            clearTimeout(tm_popup);
            
            var $this=$(this),
                $target=$($this.data('popup-open'));
                
            $('html').addClass('popup-view');
            $target.parent().addClass('active');
            $target.addClass('active');
            
            
            tm_popup=setTimeout(function(){
                $('.popup-wrap').removeClass('fading');
                
                $target.trigger('popupShown');
            }, tm_popupDuration);
            
            return false;
        });
        
        $('[data-popup-close]').click(function(){
            clearTimeout(tm_popup);
            
            $('.popup-wrap').addClass('fading');
            
            tm_popup=setTimeout(function(){
                $('.popup-wrap, .popup-item').removeClass('active');
                $('html').removeClass('popup-view');
            }, tm_popupFadeDuration);
        });
        
        $('.popup-wrap').click(function(e){
            if($(e.target).hasClass('popup-wrap')){
                $('.popup-wrap').addClass('fading');
                
                tm_popup=setTimeout(function(){
                    $('.popup-wrap, .popup-item').removeClass('active');
                    $('html').removeClass('popup-view');
                }, tm_popupFadeDuration);
            }
        });
        
    }
    popupInit();
	
	//tab init

    function tabInit(){

        this.showTab=function(control){

            var self=this,
                tm_tab=null,
                tm_tab_duration=150,
                tm_tab_fade=null,
                tm_tab_fade_duration=30;

            $control=$(control),
                $parent=$control.parents('[data-tabcontrol-parent]'),
                $target=$('[data-tab='+$control.data('tabcontrol')+']'),
                $target_parent=$target.parents('[data-tabcontent-parent]');

            clearTimeout(tm_tab_fade);
            clearTimeout(tm_tab);

            if($control.hasClass('active')){
                return;
                /*$parent.find('[data-tabcontrol]').removeClass('active');
                 $target_parent.find('[data-tab]').addClass('fade');

                 tm_tab=setTimeout(function(){
                 $target_parent.find('[data-tab]').removeClass('active');
                 }, tm_tab_duration);*/
            }
            else{
                $parent.find('[data-tabcontrol]').removeClass('active');
                $control.addClass('active');

                $target_parent.find('[data-tab]').not($target).addClass('fading');

                tm_tab=setTimeout(function(){
                    $target_parent.find('[data-tab]').not($target).removeClass('active');
                    $target.addClass('active');
                    $target.trigger('tab-shown');
                    tm_tab_fade=setTimeout(function(){
                        $target.removeClass('fading');
                    }, tm_tab_fade_duration);
                }, tm_tab_duration);
            }

        }

        $('[data-tabcontrol]').not('.active').click(function(){
            self.showTab(this);
        });

        $('[data-tabcontrol-parent]').each(function(){
            self.showTab($(this).find('[data-tab-autoshow]').first());
        });
    }
    tabInit();
    
   
});    