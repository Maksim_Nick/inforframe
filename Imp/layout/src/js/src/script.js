$(function () {
    $('.slider-product .owl-carousel').owlCarousel({
        items:4,
        loop:true,
        nav:true,
    });
    $('.slider-main .owl-carousel').owlCarousel({
        items:1,
        loop:true,
        autoplay:true,
        autoplayTimeout:3000,
        animateOut: 'fadeOut',
    });
    if ($('.card-box_img').length >= 1)
    {
        if ($('.card-box_img img').width() <= 560) {
            $('.card-box_img ').addClass('card-box_img_min');
        }
    }

    // basket script
    $('[data-count-control]').click(function(){
        var $this=$(this),
            $parent=$this.parents('[data-count-item]'),
            $target=$parent.find('[data-count-target]');

            $target.val(parseInt($target.val())+parseInt($this.data('count-control')));
            if($target.val() <= 1 )
            {
                $target.val(1);
            }
            var $targetVal = $parent.parents('.basket-box_item'),
                $val = $targetVal.find('.basket-box_summ [data-count-val]'),
                summ = ((parseInt($target.val()))*(parseInt($val.data('count-val'))));
            parse(summ,$val);
            summFunc();
    });
    function parse(parseVal,selector){
        var summParse = parseVal.toString().replace(/(\d{2,3})(?=((\d{0})*([^\d]|$)))/g, " $1 ");
        selector.text(summParse);
    }
    function summFunc(){
        var summVal = 0 ;
        $('[data-count-val]').each(function(){
            summVal+=parseInt($(this).text().replace(" ",""));
        });
        parse(summVal,$('[data-count-summ]'));
    }
    if ($('[data-count-val]').length >= 1){
        parse($('[data-count-val]').data('count-val'),$('[data-count-val]'));
    }
    summFunc();

    //basket script End

    //map
    var map;

    function initialize() {
        var pos1 = {lat: 55.503739, lng: 37.560621},
            pos2 = {lat: 55.671914, lng: 37.572899};

        var mapOptions = {
            center: pos1,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        },
        markerImage = new google.maps.MarkerImage(
            'img/marker-map.png',
            new google.maps.Size(24, 32),
            new google.maps.Point(0, 0),
            new google.maps.Point(12, 32)
        );
        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        var marker = new google.maps.Marker({
            icon: markerImage,
            map: map,
            position: pos1,
            title: 'Imperia Doors'
        });
        var marker = new google.maps.Marker({
            icon: markerImage,
            map: map,
            position: pos2,
            title: 'Imperia Doors'
        });
        if ($('#map-1').length >= 1){
            google.maps.event.addDomListener(document.getElementById('map-1'),'click', function() {
                map.panTo(pos1)
            });
            google.maps.event.addDomListener(document.getElementById('map-2'),'click', function() {
                map.panTo(pos2)
            });
        }
    }
    if($('#map').length >= 1){
        initialize();
    }
    $('.map-selection .map-box_item').click(function () {
        $('.map-selection .map-box_item').removeClass('active');
        $(this).addClass('active');
    });
    // color box
    
});
