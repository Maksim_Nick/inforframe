var pathSrc='./layout/src/',
    pathBuild='./layout/build/';
module.exports = {
    entry: pathSrc+'js/entry.js',
    output: {
        path: pathBuild+'js',
        filename: 'app.js'
    }
};