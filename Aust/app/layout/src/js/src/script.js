$(function () {
    $(".process-box_content .nano").nanoScroller({ sliderMaxHeight: 120 });
    $(".question-box .nano").nanoScroller({ sliderMaxHeight: 150 });
    $('[data-phone]').click(function () {
        $('[data-phone]').removeClass('active')
        var $this = $(this);
        $('[data-phone='+$this.data('phone')+']').addClass('active');
    });
    $(".lang-switch").click(function () {
        var $this = $(this);
        $this.toggleClass('active');
    });
    $("a.anchor").click(function() {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);
        return false;
    });
    $('.header-top_menu-nav-mobile').click(function(){
        $('.header-top_menu-nav').toggleClass('active');
        $('body').toggleClass('active');
    });
    $('.slider-box').bxSlider({
        pagerCustom: '.slider-pager',
        controls:false,
    });
    function tabInit(){

        this.showTab=function(control){

            var self=this,
                tm_tab=null,
                tm_tab_duration=150,
                tm_tab_fade=null,
                tm_tab_fade_duration=30;

            $control=$(control),
                $parent=$control.parents('[data-tabcontrol-parent]'),
                $target=$('[data-tab='+$control.data('tabcontrol')+']'),
                $target_parent=$target.parents('[data-tabcontent-parent]');

            clearTimeout(tm_tab_fade);
            clearTimeout(tm_tab);

            if($control.hasClass('active')){
                return;
            }
            else{
                $parent.find('[data-tabcontrol]').removeClass('active');
                $control.addClass('active');

                $target_parent.find('[data-tab]').not($target).addClass('fading');

                tm_tab=setTimeout(function(){
                    $target_parent.find('[data-tab]').not($target).removeClass('active');
                    $target.addClass('active');
                    $target.trigger('tab-shown');
                    tm_tab_fade=setTimeout(function(){
                        $target.removeClass('fading');
                    }, tm_tab_fade_duration);
                }, tm_tab_duration);
            }

        }

        $('[data-tabcontrol]').not('.active').click(function(){
            self.showTab(this);
        });

        $('[data-tabcontrol-parent]').each(function(){
            self.showTab($(this).find('[data-tab-autoshow]').first());
        });
    }
    tabInit();
});