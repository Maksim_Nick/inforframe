$(function() {
function popupInit(){
        
        var tm_popup=null,
            tm_popupDuration=30,
            tm_popupFadeDuration=300;
            
        $('[data-popup-open]').click(function(e){
        
            e.preventDefault();
            
            clearTimeout(tm_popup);
            
            var $this=$(this),
                $target=$($this.data('popup-open'));
                
            $('html').addClass('popup-view');
            $target.parent().addClass('active');
            $target.addClass('active');
            
            
            tm_popup=setTimeout(function(){
                $('.popup-wrap').removeClass('fading');
                
                $target.trigger('popupShown');
            }, tm_popupDuration);
            
            return false;
        });
        
        $('[data-popup-close]').click(function(){
            clearTimeout(tm_popup);
            
            $('.popup-wrap').addClass('fading');
            
            tm_popup=setTimeout(function(){
                $('.popup-wrap, .popup-item').removeClass('active');
                $('html').removeClass('popup-view');
            }, tm_popupFadeDuration);
        });
        
        $('.popup-wrap').click(function(e){
            if($(e.target).hasClass('popup-wrap')){
                $('.popup-wrap').addClass('fading');
                
                tm_popup=setTimeout(function(){
                    $('.popup-wrap, .popup-item').removeClass('active');
                    $('html').removeClass('popup-view');
                }, tm_popupFadeDuration);
            }
        });
        
    }
    popupInit();
	
	//tab init
    
   
});    