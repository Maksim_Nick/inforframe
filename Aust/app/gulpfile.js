var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var spritesmith = require('gulp.spritesmith');
var webpack = require('webpack');
var browserSync = require('browser-sync').create();

var pathSrc='./layout/src/',
    pathBuild='./layout/build/';


gulp.task('sass', function(){
    return gulp.src(pathSrc+'scss/style.scss')
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest(pathBuild+'css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('jade', function(){
    gulp.src(pathSrc+'jade/src/**/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest(pathBuild+'/'))
});

gulp.task("webpack", function(callback) {
    // run webpack
    webpack(require('./webpack.config'), function(err, stats) {
        if(err) throw new gutil.PluginError('webpack', err);
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
        callback();
    });
});

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'layout/build'
        }
        /*
         browser: ["google chrome", "firefox"]*/
    });
});

gulp.task('fonts', function () {
    gulp.src(pathSrc+'fonts/**/')
        .pipe(gulp.dest(pathBuild+'fonts/'))
});

gulp.task('img', function () {
    gulp.src(pathSrc+'img/**/')
        .pipe(gulp.dest(pathBuild+'img/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('sprite', function() {
    var spriteData =
        gulp.src(pathSrc+'img/sprite/**/*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.css',
            }));
    spriteData.img.pipe(gulp.dest(pathSrc+'img/')) // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest(pathBuild+'img/')) // путь, куда сохраняем стили
});



gulp.task('watch', ['browserSync', 'jade', 'sass', 'webpack','fonts','img'], function(){
    gulp.watch('layout/src/scss/**/*', ['sass']);
    gulp.watch('layout/src/js/**/*.js', ['webpack']);
    gulp.watch('layout/src/jade/**/*.jade', ['jade']);
    gulp.watch('layout/src/fonts/**/*', ['fonts']);
    gulp.watch('layout/src/img/**/*', ['img']);

    gulp.watch('layout/build/bundle/js/**/*', browserSync.reload);
    gulp.watch('layout/build/**/*', browserSync.reload);
});
